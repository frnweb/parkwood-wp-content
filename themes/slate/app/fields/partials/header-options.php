<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$optionsheader = new FieldsBuilder('header_options');

$optionsheader

->setLocation('options_page', '==', 'theme-header-settings');

$optionsheader
    ->addTab('logo', ['placement' => 'left'])

		//Logo checkbox
		->addTrueFalse('add_logo')

	    //Logo 
		->addImage('logo', [
			'label' => 'Logo',
			'ui' => $config->ui
		])
	    ->conditional('add_logo', '==', 1)

	    //Mobile Menu Logo checkbox
		->addTrueFalse('add_menulogo', [
			'label' => 'Add Logo to Mobile Menu'
	    ])

	    //Mobile Menu Logo 
		->addImage('menulogo', [
			'label' => 'Mobile Menu Logo',
			'ui' => $config->ui
		])
	    ->conditional('add_menulogo', '==', 1);

$optionsheader
    ->addTab('Google Translate', ['placement' => 'left'])

	    //Google Translate
		->addTrueFalse('add_translate', [
			'label' => 'Add Google Translate to Utility Nav'
	    ])

	    //Google Translate Script 
		->addWysiwyg('translate_script', [
	        'label' => 'Add Javascript for Translate Button',
	        'tabs' => 'text',
	        'toolbar' => 'basic',
	        'media_upload' => 0,
	    ])
	    ->conditional('add_translate', '==', 1);

$optionsheader
    ->addTab('Notification Bar', ['placement' => 'left'])

		->addText('notification_text', [
			'wrapper' => ['width' => 70]
		])

		//Button
		->addLink('notification_button', [
			'wrapper' => ['width' => 30]
		]);
    
return $optionsheader;
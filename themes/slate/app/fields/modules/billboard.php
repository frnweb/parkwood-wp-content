<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],

];

$billboard = new FieldsBuilder('billboard');

$billboard
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));


$billboard
	
	->addTab('content', ['placement' => 'left'])

		->addFields(get_field_partial('modules.card'));

$billboard

	->addTab('background', ['placement' => 'left'])

		//Image 
		->addGroup('billboard_image')

			//Large Image 
			->addImage('large', ['wrapper' => ['width' => 50]])
				->setInstructions('Image background for billboard medium breakpoint and up')

			//Small Image 
			->addImage('small', ['wrapper' => ['width' => 50]])
				->setInstructions('Image background for billboard small breakpoint')
			
		->endGroup();

return $billboard;
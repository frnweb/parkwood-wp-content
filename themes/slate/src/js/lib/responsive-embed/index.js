import $ from 'jquery'

//Adds responsive embed to all iframes from youtube
jQuery(function($) {
    jQuery(document).ready(function() {
		var vidDefer = document.getElementsByTagName('iframe');
		
		// Remove empty P tags created by WP inside of Accordion and Orbit
		$('.accordion p:empty, .orbit p:empty').remove();

		$("p").find("iframe").unwrap();

		for (var i=0; i<vidDefer.length; i++) {
			if(vidDefer[i].getAttribute('data-src')) {
			vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
			} 
		}

		if(!$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').parents("div").hasClass("sl_reveal")) {
			$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
				if ( $(this).innerWidth() / $(this).innerHeight() > 1.5 ) {
					$(this).wrap("<div class='widescreen responsive-embed'/>");
				} else {
					$(this).wrap("<div class='responsive-embed'/>");
				}
			})
		}
	});
});